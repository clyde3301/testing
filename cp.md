# HNG(3) in 480

### Processor
- **Type**: `4.77 MHz Zentech Z80`
- **Architecture**: `8-bit processing unit with 16-bit data path`

### Memory
- **RAM**: `16 KB, expandable to 64 KB with Zentech memory modules`

### Storage
- **Primary Storage**: `One 5.25" Z-Disk drive`
- **Capacity**: `160 KB per disk`

### Display
- **Size**: `9-inch`
- **Type**: `Monochrome Z-Display LCD`
- **Resolution**: `400x300 pixels`


# StarT-485

### Processor
- **Type**: `8 MHz Zentech Z80 Plus`
- **Architecture**: `8-bit processing unit with 32-bit data path`

### Memory
- **RAM**: `64 KB, expandable to 256 KB with Zentech memory modules`

### Storage
- **Primary Storage**: `Two 5.25" Z-Disk II drives`
- **Capacity**: `320 KB per disk`

### Display
- **Size**: `12-inch`
- **Type**: `Monochrome Z-Display II LCD`
- **Resolution**: `640x480 pixels`

# HNG(X) in 490

### Processor
- **Type**: `Zentech Z-Core 120 MHz`
- **Architecture**: `32-bit RISC processor`
- **Cache**: `512 KB L2 Cache`

### Memory
- **RAM**: `128 MB SD-RAM, expandable to 512 MB`
- **ROM**: `4 MB Flash ROM with full system BIOS`

### Storage
- **Primary Storage**: `3.5" Zentech Z-Drive`
- **Capacity**: `1.44 MB per diskette`
- **Secondary Storage**: `20 GB Zentech Z-Hard Disk`
- **Interface**: `Ultra DMA with S.M.A.R.T capability`

### Display
- **Size**: `17-inch`
- **Type**: `Z-Display Color CRT`
- **Resolution**: `800x600 pixels`
